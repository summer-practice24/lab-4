#include <iostream>

using namespace std;

double newtonMethod(double, double);
double function(double);
double derivative(double);

int main()
{
	double root;
	double epsilon;

	while (true)
	{
		cout << "Enter the initial approximation and accuracy: ";
			cin >> root >> epsilon;
		if ((epsilon < 1) && (epsilon > 0)) break;
		cout << "Accuracy is incorrect. Try again." << endl;
	}
	system("cls");
	cout << endl << "The root of the equation is equal " << newtonMethod(root, epsilon) << endl;
	system("pause");
	return 0;
}


double newtonMethod(double previous, double epsilon)
{
	double next = previous - function(previous) / derivative(previous);

	while (fabs(previous - next) >= epsilon)
	{
		previous = next;
		next = previous - function(previous) / derivative(previous);
	}

	return next;
}

double function(double x)
{
	return 3 * x - cos(x) - 1;
}

double derivative(double x)
{
	return 3 + sin(x);
}